(defsystem "astral"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on (:iterate :cl-utilities)
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "corrector")
                 (:file "pcg")
                 (:file "keyspace")
                 (:file "maze")
                 (:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "astral/tests"))))

(defsystem "astral/tests"
  :author ""
  :license ""
  :depends-on ("astral"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for astral"
  :perform (test-op (op c) (symbol-call :rove :run c)))
