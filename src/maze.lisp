(in-package :astral)
(defvar *ns*)
(defvar *we)
(defvar *se*)
(defvar *sw*)
(defvar *ne*)
(defvar *se*)

(setf *ns* "║")
(setf *we "═")
(setf *se* "╔")
(setf *sw* "╗")
(setf *ne* "╚")
(setf *se* "╝")

(defvar *row1*)
(defvar *row2*)
(setf *row1* #())
