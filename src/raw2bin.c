#include <stdio.h>

int main(argc,argv)
int argc ;
char *argv[] ;

{
	FILE *fpin, *fpout;
	long x, y;
	int i, j;
	unsigned long lx;
	unsigned int ni, nl;
	union UI {
		long x;
		unsigned char cx[4];
	} ux;

	if (argc != 3) {
		fprintf(stderr, "Usage: raw2bin <infile> <outfile>\n\n\
			where <infile> is a space separated ascii file of \
			integers (longs)\nand <outfile> is an binary file of \
			binary numbers corresponding\nto <infile>.\n");
		exit(1);
	}
	ni = sizeof(int);
	nl = sizeof(long);
	if ((fpin = fopen(argv[1], "r")) == NULL) {
		fprintf(stderr, "%s can not be opened for reading\n",argv[1]);
		exit(1);
	}
	if ((fpout = fopen(argv[2], "wb")) == EOF) {
		fprintf(stderr, "%s can not be opened for writing\n",argv[2]);
		exit(1);
	}

	i = 0;
	ux.x = 0;
	while (fscanf(fpin, "%ld", &ux.x) != EOF) {
		i++;
		lx = (unsigned long)ux.x;
		for (j=0; j<4; j++) {
			if ((y=fputc(ux.cx[j], fpout)) == EOF) {
				fprintf(stderr, "error on writing to %s\n",
					argv[2]);
			}
		}
		ux.x = 0;
	}

	fclose(fpin);
	fclose(fpout);
}
