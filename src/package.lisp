(defpackage astral
  (:use :cl :iterate :sb-rotate-byte)
  (:export
   #:encrypt
   #:decrypt))
