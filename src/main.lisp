(in-package :astral)

(defvar *code-list*)
(setf *code-list* #("SILVER" "PICTURE" "CHESHIRE" "DANTE"
                    "ROTATE" "WINDING" "TERRA" "DIAMOND"
                    "AVID" "PHOBOS" "ESQUIRE" "INFER"
                    "NERO" "BITTER" "AMBER" "MONO"
                    "NOBLE" "RAVEN" "SOLAR" "CHALLENGE"
                    "SONNET" "UNION" "FALCON" "SATURN" "PARADE"
                    "MIRAGE" "PRECIOUS" "BROKEN" "ARIES" "PROGRAM"
                    "RESOURCE" "SAPPHIRE" "COMET"
                    "RUBY" "POETRY" "CANDLE" "FERROUS"
                    "AEONS" "SERPENT" "PLUTO" "SILENT" "TOWER"
                    "GLIMMER" "TIGER" "ENTER" "TRIPLE"
                    "VENUS" "IDOL" "DOUGLAS" "GOLEM" "SEVEN"
                    "ESTATE" "PETAL" "INGRESS" "ASTRO" "TRIGGER"
                    "WATSON" "GEMSTONE" "IMPLY"
                    "CASTLE" "PATTERN" "SALMON" "RESPOND"
                    "FIFTY" "DOUBLE" "ADMIN" "IRIS"
                    "HUNTER" "NOVA" "GYPSUM" "AUTHOR" "CHEETAH"
                    "TWENTY" "INSECT" "CADENCE"
                    "SHAMROCK" "AZTEC" "SOLO" "SHALLOW" "DANGER"
                    "GENRE" "ARCTIC" "WIZARD"
                    "DAISY" "TANGENT" "CAUSTIC" "WINDOW"
                    "TAURUS" "SPARKLE" "TULIP" "DHARMA"
                    "FOUNTAIN" "EMPIRE" "MOTION" "PHOENIX"
                    "CORAL" "EFFECT" "LINEAR" "CIRCUIT"
                    "DISCO" "ACORN" "FOLLY" "ORPHAN" "COBRA" "PILGRIM"
                    "NEUTRAL" "TRUMPET" "HORNET"
                    "VAMPIRE" "BISHOP" "COPPER" "NOVEL" "SCARLET"
                    "FIDDLE" "TOKEN" "OYSTER"
                    "CELLO" "MOZART" "DUAL" "BANJO" "HEMLOCK"
                    "REPLY" "SOLVENT" "OBOE" "PARLOR" "ORCHARD"
                    "BLINDING" "OMEN" "CYMBAL" "VEGA"
                    "OPAL" "COSMIC" "EXILE" "NEPTUNE"
                    "TAXI" "WISDOM" "OPUS" "TALON"))

(defun concat-string (list)
  "A non-recursive function that concatenates a list of strings."
  (if (listp list)
      (with-output-to-string (s)
        (dolist (item list)
          (if (stringp item)
              (format s "~a" item))))))

(defstruct ctx
  the-key
  was-garbage
  output
  the-rng
  (char-count 0)
  (ff 0)
  (the-buckets (make-array 16 :element-type '(unsigned-byte 32)
                           :initial-element 0)
               :type (simple-array (unsigned-byte 32) (16))))

(defun append-output (what ctx)
  (push what (ctx-output ctx)))

(defun get-rn (ctx)
  (gen-rnd-n (ctx-the-rng ctx)))

(defun modify-and-get-rn (what ctx)
  (update-mult-gen-rnd-n what (ctx-the-rng ctx)))

(defun inc-nth-bucket (index value ctx)
  (incf-wrap32 (elt (ctx-the-buckets ctx) index) value))

(defun make-coder (key)
  (let ((ctx (make-ctx)))
    (setf (ctx-the-key ctx) (gen-key-ctx key))
    (setf (ctx-the-rng ctx) (make-pcg-ctx (get-n-bit-key 256 (ctx-the-key ctx))))
    ctx))

(defmacro update-buckets ()
  `(progn
     (inc-nth-bucket (mod (+ bucket-index 1) 16) (rot32-ish (logeqv ff (logxor rnd-n code-pos))) ctx)
     (inc-nth-bucket (mod (+ bucket-index 2) 16)
                     (+
                      (modify-and-get-rn
                       (+ (key-ctx-t1 (ctx-the-key ctx))
                          (rot32-ish (key-ctx-t2 (ctx-the-key ctx)))
                          (key-ctx-t3 (ctx-the-key ctx))
                          (rot32-ish (key-ctx-t4 (ctx-the-key ctx)))
                          (key-ctx-t5 (ctx-the-key ctx)))
                       ctx)
                      (logeqv code bucket-index))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 3) 16)
                     (logxor (key-ctx-t1 (ctx-the-key ctx))
                             plain
                             code)
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 4) 16)
                     (rot32-ish (+ (key-ctx-t5 (ctx-the-key ctx))
                                   ff (+ bucket-index 4)))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 5) 16)
                     (+ (key-ctx-t2 (ctx-the-key ctx))
                        (logxor (get-rn ctx)
                                (+ bucket-index 5)))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 6) 16)
                     (+ (key-ctx-t4 (ctx-the-key ctx))
                        (logxor (get-rn ctx)
                                (+ plain rnd-n)))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 7) 16)
                     (+ (key-ctx-t3 (ctx-the-key ctx))
                        (logeqv (get-rn ctx)
                                (+ code rnd-n)))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 8) 16)
                     (+ (key-ctx-t3 (ctx-the-key ctx))
                        (rot32-ish (key-ctx-t1 (ctx-the-key ctx)))
                        (logeqv (get-rn ctx)
                                (logxor code rnd-n)))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 9) 16)
                     (+ (key-ctx-t2 (ctx-the-key ctx))
                        (key-ctx-t5 (ctx-the-key ctx))
                        (logeqv (get-rn ctx)
                                (logxor plain rnd-n)))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 10) 16)
                     (+ (key-ctx-t4 (ctx-the-key ctx))
                        (rot32-ish (key-ctx-t2 (ctx-the-key ctx)))
                        (logxor (get-rn ctx)
                                (logxor code rnd-n)))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 11) 16)
                     (+ (key-ctx-t3 (ctx-the-key ctx))
                        (key-ctx-t4 (ctx-the-key ctx))
                        (elt (ctx-the-buckets ctx)
                             (mod (+ bucket-index 1) 16)))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 12) 16)
                     (+ (key-ctx-t5 (ctx-the-key ctx))
                        (key-ctx-t1 (ctx-the-key ctx))
                        (rot32-ish (elt (ctx-the-buckets ctx)
                                        (mod (+ bucket-index 2) 16))))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 13) 16)
                     (+ (key-ctx-t2 (ctx-the-key ctx))
                        (key-ctx-t3 (ctx-the-key ctx))
                        (rot32-ish (elt (ctx-the-buckets ctx)
                                        (mod (+ bucket-index 3) 16))))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 14) 16)
                     (+ (logxor (key-ctx-t1 (ctx-the-key ctx))
                                (rot32-ish (key-ctx-t3 (ctx-the-key ctx))))
                        (elt (ctx-the-buckets ctx)
                             (mod (+ bucket-index 4) 16)))
                     ctx)
     (inc-nth-bucket (mod (+ bucket-index 15) 16)
                     (+ (logxor (key-ctx-t2 (ctx-the-key ctx))
                                (key-ctx-t4 (ctx-the-key ctx)))
                        (elt (ctx-the-buckets ctx)
                             (mod (+ bucket-index 5) 16)))
                     ctx)))

(defun get-output (ctx)
  (concat-string (ctx-output ctx)))

(defun encode (plain ctx)
  (iter (for ch in-string plain)
        (encode-outer (char-code ch) ctx))
  (values (get-output ctx) ctx))

(defun encode-outer (p ctx)
  (let ((rnd-n (get-rn ctx))
        (garbage ""))
    (if (= 0 (mod rnd-n 2))
        (setf garbage (concatenate 'string (elt *code-list*
                                                (mod rnd-n
                                                     (length *code-list*)))
                                   " ")))
    (append-output garbage ctx)
    (encode-inner p ctx)
    ctx))

(defun encode-inner (plain ctx)
  (let* ((c-pos (ctx-char-count ctx))
         (code (next-key (ctx-the-key ctx)))
         (code-pos (key-ctx-key-count (ctx-the-key ctx)))
         (ff (ctx-ff ctx))
         (rnd-n (get-rn ctx))
         (bucket-index (mod (+ rnd-n code code-pos) 16))
         (bucket-value (elt (ctx-the-buckets ctx) bucket-index))
         (cipher-number (mod (+ code
                                (logior ff bucket-value)
                                plain
                                (logxor rnd-n (get-rn ctx))
                                c-pos)
                             (length *code-list*))))
    ;; (print (+ code
    ;;           (logior ff bucket-value)
    ;;           plain
    ;;           (logxor rnd-n (get-rn ctx))
    ;;           c-pos))
    (inc-nth-bucket bucket-index (+ code (logxor bucket-value ff)) ctx)
    (incf-wrap32 (ctx-char-count ctx) 1)
    (incf-wrap32 (ctx-ff ctx)
                 (shl (logxor rnd-n bucket-value ff cipher-number)
                      75
                      32))
    (update-buckets)
    (append-output " " ctx)
    (append-output (elt *code-list* cipher-number)  ctx)
    ctx))

(defun conc (a b)
  (concatenate 'string (string b) (string a)))

(defun decode (cipher ctx)
  (let* ((tmp (cl-utilities:split-sequence
              #\Space (string-trim " " cipher)))
        (cipher-corrected
         (reverse
          (make-array (length tmp)
                      :initial-contents tmp))))
    (iter (for word in-sequence cipher-corrected)
      (decode-outer (position word *code-list* :test #'string=) ctx))
    (values (reduce #'conc (ctx-output ctx)) ctx)))

(defun decode-outer (c ctx)
  (if (ctx-was-garbage ctx)
      (progn
        (setf (ctx-was-garbage ctx) nil)
        (decode-inner c ctx)
        ctx)
      (let ((rnd-n (get-rn ctx)))
        (if (= 0 (mod rnd-n 2))
            (setf (ctx-was-garbage ctx) rnd-n)
            (decode-inner c ctx))
        ctx)))

(defun decode-inner (cipher ctx)
  (let* ((c-pos (ctx-char-count ctx))
         (code (next-key (ctx-the-key ctx)))
         (code-pos (key-ctx-key-count (ctx-the-key ctx)))
         (ff (ctx-ff ctx))
         (rnd-n (get-rn ctx))
         (bucket-index (mod (+ rnd-n code code-pos) 16))
         (bucket-value (elt (ctx-the-buckets ctx) bucket-index))
         (plain (mod (- cipher (+ code
                                         (logior ff bucket-value)

                                         (logxor rnd-n (get-rn ctx))
                                         c-pos))
                             (length *code-list*))))
    (inc-nth-bucket bucket-index (+ code (logxor bucket-value ff)) ctx)
    (incf-wrap32 (ctx-char-count ctx) 1)
    (incf-wrap32 (ctx-ff ctx)
                 (shl (logxor rnd-n bucket-value ff cipher)
                      75
                      32))
    (update-buckets)
    (append-output (code-char plain) ctx)
    ctx))



(defun encrypt (key plain)
  (multiple-value-bind (out ctx)
      (encode plain (make-coder key))
    (print ctx)
    out))


(defun decrypt (key cipher)
  (multiple-value-bind (out ctx)
      (decode cipher (make-coder key))
    out))
