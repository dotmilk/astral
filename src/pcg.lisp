(in-package :astral)

(declaim (inline shl))
(defun shl (x width bits)
  "Compute bitwise left shift of x by 'bits' bits, represented on 'width' bits"
  (logand (ash x bits)
          (1- (ash 1 width))))
(declaim (inline shr))
(defun shr (x width bits)
  "Compute bitwise right shift of x by 'bits' bits, represented on 'width' bits"
  (logand (ash x (- bits))
          (1- (ash 1 width))))

(defstruct pcg-state
  (state 0 :type (unsigned-byte 512))
  (multiplier 6364136223846793005 :type (unsigned-byte 512))
  (increment 1442695040888963407  :type (unsigned-byte 512)))

;; rng->state * PCG_DEFAULT_MULTIPLIER_64 + rng->inc
(defun step-rng (rng)
  (setf (pcg-state-state rng)
        (ldb (byte 512 0)
             (+ (* (pcg-state-state rng)
                   (pcg-state-multiplier rng))
                (pcg-state-increment rng)))))


;; pcg_rotr_32(((state >> 18u) ^ state) >> 27u, state >> 59u)
(defun rng-output (state)
  (rotate-byte (* -1 (shr state 512 507))
               (byte 256 0)
               (shr (logxor (shr state 512 130)
                            state)
                    512 251)))

(defun gen-rnd-n (rng)
  (let ((old-state (pcg-state-state rng)))
    (step-rng rng)
    (rng-output old-state)))

(defun update-mult-gen-rnd-n (inc-by rng)
  (setf (pcg-state-multiplier rng)
        (ldb (byte 512 0) (+ (pcg-state-multiplier rng) inc-by)))
  (gen-rnd-n rng))

(defun make-pcg-ctx (seed)
  (let ((ctx (make-pcg-state)))
    (setf (pcg-state-state ctx) seed)
    ctx))
