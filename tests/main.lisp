(defpackage astral/tests/main
  (:use :cl
        :astral
        :rove))
(in-package :astral/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :astral)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
